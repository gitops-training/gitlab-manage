# Gitlab Manage

Projeto responsável por criar e configurar projetos e grupos no Gitlab.

O projeto cria os seguintes grupos e projetos:

```
gitops-training
├── apps
│   └── cluster-management
└── infrastructure
    └── aws
```

Variáveis de ambiente usadas:

Nome | Descrição | Valor padrão
--- | --- | ---
`TF_CLI_CONFIG_FILE` | Credenciais de acesso para o backend. |
`GITLAB_TOKEN` | Token de acesso Gitlab. |
